const {app,BrowserWindow,ipcMain,dialog,net} = require('electron')
const path  = require('path');

process.env["ELECTRON_DISABLE_SECURITY_WARNINGS"] = "true";

//监听渲染进程发送的事件
ipcMain.on('lc-message',(event,args)=>{
  event.reply('lc-reply',"这是主进程的答复")
  console.log(args)
})

//监听渲染进程发送的消息,要求打开新窗口
ipcMain.on("openNewWindow",(event,args)=>{
  createNewWindow("https://www.taobao.com")
  createNewWindow("https://www.baidu.com")
})

const createNewWindow = (url)=>{
  const mainWindow = new BrowserWindow({
    width:800,
    height:600,
    webPreferences:{
      nodeIntegration:true,//浏览器js支持node
    }
  })
  //此处__dirname路径是一直到src
  mainWindow.loadURL(url)
  //打开devtools
  mainWindow.webContents.openDevTools();
}

const createWindow = ()=>{
  const mainWindow = new BrowserWindow({
    width:800,
    height:600,
    webPreferences:{
      webSecurity:false, //允许跨域
      nodeIntegration:true,//浏览器js支持node
      contextIsolation:false, //是否在独立 JavaScript 环境中运行 Electron API和指定的preload 脚本
      webviewTag:true,//设置支持webview
    }
  })
  //此处__dirname路径是一直到src
  mainWindow.loadFile(path.join(__dirname,'index.html'))
  //打开devtools
  mainWindow.webContents.openDevTools();

  setTimeout(()=>{
    //主线程发消息给渲染进程
    mainWindow.webContents.send('lc-active',"创建窗口之后,主进程发送数据给渲染进程")

    //打开一个弹窗
    // "openFile" 允许选择文件
    // "openDirectory" 允许选择文件夹
    // "multiSelections" 
    // "showHiddenFiles" 
    // "createDirectory" 
  //   dialog.showOpenDialog({
  //     properties:['openFile',"multiSelections"]
  //   }).then((result)=>{
  //     console.log("result",result)
  //   })

  //   mainWindow.on("close",e=>{
  //     e.preventDefault()
  //     dialog.showMessageBox(mainWindow,{
  //       type:"warning",
  //       title:"关闭",
  //       message:"是否要关闭窗口?",
  //       buttons:['取消','残酷关闭']
  //     }).then((index)=>{
  //       console.log(index)
  //       if(index.response==1){
  //         app.exit()
  //       }
  //     })
  //   })
  },2000)

  //原生网络请求库
  let request = net.request("http://www.taobao.com")
  request.on("response",(response)=>{
    console.log("response",response)
    response.on("data",(chunk)=>{
      console.log(chunk)
    })
  })
  request.end()

}

//应用就绪时调用的函数
app.whenReady().then(()=>{
  createWindow()
})
