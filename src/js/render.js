const fs = require('fs');

// let holder  = document.querySelector('#holder')
// let readList  = document.querySelector('#readList')
// holder.addEventListener("drop",e=>{
//   e.preventDefault()
//   e.stopPropagation()
//   for(const file of e.dataTransfer.files){
//     console.log(file,file.path)
//     fs.readFile(file.path,(err,data)=>{
//       if(err){
//         console.log(err)
//       }else{
//         let newDiv = document.createElement("div")
//         newDiv.className = 'readFile'
//         newDiv.innerHTML = `
//           <h3>${file.name}</h3>
//           <pre>${data}</pre>
//         `
//         readList.appendChild(newDiv)
//       }
//     })
//   }
// })

// holder.addEventListener('dragover',e=>{
//   e.preventDefault()
//   e.stopPropagation()
// })

const webview = document.querySelector("webview")
webview.addEventListener("did-start-loading",()=>{
  console.log("正在加载中...")
})
webview.addEventListener("did-stop-loading",()=>{
  console.log("结束加载...")
  console.log([webview])
  setTimeout(()=>{
    const input = document.querySelector("#kw")
    const btn = document.querySelector("#su")
    input.value = '猪猪读书'
    btn.click()
  },2000)
  webview.insertCSS(`#su{background:red !important}`)
  webview.executeJavaScript(`  setTimeout(()=>{
    const input = document.querySelector("#kw")
    const btn = document.querySelector("#su")
    input.value = '猪猪读书'
    btn.click()
  },2000)`)
})